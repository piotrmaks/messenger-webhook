'use strict';

const PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN;
const VERIFY_TOKEN = process.env.VERIFY_TOKEN;

var notificationQueue = [];

// Imports dependencies and set up http server
const
  express = require('express'),
  bodyParser = require('body-parser'),
  request = require('request'),
  moment = require('moment'),
  app = express().use(bodyParser.json()); // creates express http server

function firstEntity(nlp, name) {
  return nlp && nlp.entities && nlp.entities[name] && nlp.entities[name][0];
}

function isTimestampNow(timestamp) {
  return timestamp.isSame(moment().utc(), 'second');
}

function sayHello(sender_psid) {
  let response;

  response = { "text": 'Witaj! Powiedz o czym mam Ci przypomnieć' }
  callSendAPI(sender_psid, response);
}

function sayGoodbye(sender_psid) {
  let response;

  response = { "text": 'Do widzenia!' }
  callSendAPI(sender_psid, response);
}

function sayThankyou(sender_psid) {
  let response;

  response = { "text": 'Do usług' }
  callSendAPI(sender_psid, response);
}

// Handles messages events
function handleMessage(sender_psid, received_message) {
  let response;

  // Checks if the message contains text
  if (received_message.text) {
    // Create the payload for a basic text message, which
    // will be added to the body of our request to the Send API
    response = {
      "text": `Niestety nie jestem w stanie Ci pomóc.`
    }
  }
const timestamp = firstEntity(received_message.nlp, 'datetime');
const intent = firstEntity(received_message.nlp, 'intent');
 if (timestamp && intent) {
    response = { "text": 'Ok, powiadomię Cię gdy nadejdzie czas' }
    console.log('Timestamp: ' + timestamp.value);
    console.log('Intent value: ' + intent.value);
    notificationQueue.push({psid:sender_psid,time:moment.utc(timestamp.value), msg:intent.value});
    notificationQueue.sort(function(a, b){return a.time.isBefore(b.time);});
}

const greetings = firstEntity(received_message.nlp, 'greetings');
 if (greetings) {
    if (greetings.value == 'true') {
       sayHello(sender_psid);
       return;
    }
}

const goodbye = firstEntity(received_message.nlp, 'goodbye');
 if (goodbye) {
    if (goodbye.value == 'true') {
       sayGoodbye(sender_psid);
       return;
    }
}

const thank = firstEntity(received_message.nlp, 'thank');
 if (thank) {
    if (thank.value == 'true') {
       sayThankyou(sender_psid);
       return;
    }
}

  // Send the response message
  callSendAPI(sender_psid, response);
}

// Handles messaging_postbacks events
function handlePostback(sender_psid, received_postback) {
  let response;

  // Get the payload for the postback
  let payload = received_postback.payload;

  // Set the response based on the postback payload
  if (payload === 'yes') {
    response = { "text": "Thanks!" }
  } else if (payload === 'no') {
    response = { "text": "Oops, try sending another image." }
  }
  // Send the message to acknowledge the postback
  callSendAPI(sender_psid, response);
}

// Sends response messages via the Send API
function callSendAPI(sender_psid, response) {

  // Construct the message body
  let request_body = {
    "messaging_type": "MESSAGE_TAG",
    "tag": "CONFIRMED_EVENT_REMINDER",
    "recipient": {
      "id": sender_psid
    },
    "message": response
  }

  // Send the HTTP request to the Messenger Platform
  request({
    "uri": "https://graph.facebook.com/v2.6/me/messages",
    "qs": { "access_token": PAGE_ACCESS_TOKEN },
    "method": "POST",
    "json": request_body
  }, (err, res, body) => {
    if (!err) {
      console.log('message sent! status code:' + res.statusCode)
    } else {
      console.error("Unable to send message:" + err);
    }
  });
}

// Sets server port and logs message on success
app.listen(8080, "localhost");
console.log('webhook is listening');

// Creates the endpoint for our webhook
app.post('/webhook', (req, res) => {

  let body = req.body;

  // Checks this is an event from a page subscription
  if (body.object === 'page') {

    // Iterates over each entry - there may be multiple if batched
    body.entry.forEach(function(entry) {

      // Gets the message. entry.messaging is an array, but
      // will only ever contain one message, so we get index 0
      let webhook_event = entry.messaging[0];
      console.log(webhook_event);

      // Get the sender PSID
      let sender_psid = webhook_event.sender.id;

      // Check if the event is a message or postback and
      // pass the event to the appropriate handler function
      if (webhook_event.message) {
        handleMessage(sender_psid, webhook_event.message);
      } else if (webhook_event.postback) {
        handlePostback(sender_psid, webhook_event.postback);
      }

    });

    // Returns a '200 OK' response to all requests
    res.status(200).send('EVENT_RECEIVED');
  } else {
    // Returns a '404 Not Found' if event is not from a page subscription
    res.sendStatus(404);
  }

});

// Adds support for GET requests to our webhook
app.get('/webhook', (req, res) => {

  // Parse the query params
  let mode = req.query['hub.mode'];
  let token = req.query['hub.verify_token'];
  let challenge = req.query['hub.challenge'];

  // Checks if a token and mode is in the query string of the request
  if (mode && token) {

    // Checks the mode and token sent is correct
    if (mode === 'subscribe' && token === VERIFY_TOKEN) {

      // Responds with the challenge token from the request
      console.log('WEBHOOK_VERIFIED');
      res.status(200).send(challenge);

    } else {
      // Responds with '403 Forbidden' if verify tokens do not match
      res.sendStatus(403);
    }
  }
});

function generateNotification(msg, time) {
   if(msg.includes('dentist')) {
      return { "text": "Masz wizytę u dentysty o " + time }
   }
   if(msg.includes('dermatolog')) {
      return { "text": "Masz wizytę u dermatologa o " + time }
   }
   if(msg.includes('okulista')) {
      return { "text": "Masz wizytę u okulisty o " + time }
   }
   if(msg.includes('doctor')) {
      return { "text": "Masz wizytę u lekarza o " + time }
   }
   if(msg.includes('leki')) {
      return { "text": "Pamiętaj o zażyciu leków o " + time }
   }
   if(msg.includes('tabletki')) {
      return { "text": "Połknij tabletki o " + time }
   }
   if(msg.includes('syrop')) {
      return { "text": "Pamiętaj żeby wypić syrop o " + time }
   }
   if(msg.includes('witaminy')) {
      return { "text": "Nie zapomnij zażyć witamin o " + time }
   }
   return { "text": "Chciałeś żeby Ci o tym przypomnieć: " + msg }
}

function fancyLoop() {
   if(notificationQueue.length > 0) {
      var myevent = notificationQueue.pop();
      if(isTimestampNow(myevent.time)) {
         let response;
         response = generateNotification(myevent.msg, myevent.time.utcOffset(2).format("H:mm"))
         callSendAPI(myevent.psid, response);
      } else {
         notificationQueue.push(myevent);
      }
   }
}

setInterval(fancyLoop, 500);


